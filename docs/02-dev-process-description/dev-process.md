# Детальное описание этапов разработки проекта (с иллюстрациями):

### Проектирование

Важнейший этап, с которого, зачастую, начинается какая-либо работа над программным продуктом. В нашем случае основной задачей этапа проектирования является **создание структуры БД**, имеющей необходимое (для данного проекта) кол-во сущностей, а также хорошо проработанные связи между ними. Для реализации данного этапа используем технику, которая является дефолтным методом описания структуры БД при разработке 99% программных продуктов по всему миру, - **UML-диаграммы**. *Пример:*

![00-db-structure-creation](../03-dev-process-screenshots/00-db-structure-creation.png)

------

### Создание структуры базы

Это один из первых выжнейших шагов, которые выполняются в начале этапа программирования проекта (фактически сразу после этапа проектирования). Так как в проекте мы используем *PHP7* фреймворк *Symfony 4*, удобно использовать для построения структуры базы встроенную консольную утилиту фреймворка (*bin/console*). *Данный процесс выглядит приблизительно так:*

![01-db-structure-creation](../03-dev-process-screenshots/01-db-structure-creation.png)

------

### Реализация каркаса админ-панели

![02-framework-sonata](../03-dev-process-screenshots/02-framework-sonata.png)

------

### Интеграция визуального редактора (админ-панель)



![03-wysiwyg-tinymce](../03-dev-process-screenshots/03-wysiwyg-tinymce.png)





![04-wysiwyg-tinymce-image-uploadding](../03-dev-process-screenshots/04-wysiwyg-tinymce-image-uploadding.png)









![05-wysiwyg-tinymce-image-params](../03-dev-process-screenshots/05-wysiwyg-tinymce-image-params.png)









![06-wysiwyg-tinymce-image-final](../03-dev-process-screenshots/06-wysiwyg-tinymce-image-final.png)

------

### Система локализации

```bash
php bin/console translation:update --force
```







![08-localization](../03-dev-process-screenshots/08-localization.png)







![07-localization-file-russian](../03-dev-process-screenshots/07-localization-file-russian.png)