<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Subject;
use App\Entity\TaskType;
use App\Entity\Task;
use App\Entity\Solution;
use App\Form\SolutionType;
use App\Entity\User;

class JournalController extends AbstractController
{
    /**
     * @Route("/journal", name="journal")
     */
    public function journal()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $qb = $entityManager
            ->getRepository(User::class)
            ->createQueryBuilder('u');
        $qb->join('u.groups', 'g')
            ->andWhere('g.id = :group_id')
            ->setParameter('group_id', 1);
        $users = $qb->getQuery()->getResult();

        $qb = $entityManager
            ->getRepository(Task::class)
            ->createQueryBuilder('t')
            ->orderBy('t.id');
        $tasks = $qb->getQuery()->getResult();

        $average = [];
        foreach ($users as $user) {
            $averageGrade = 0;
            $number = 0;
            $solutions = $user->getSolutions();
            foreach ($solutions as $s) {
                $grade = $s->getGrade();
                if ($grade != null) {
                    $averageGrade += $grade;
                    $number++;
                }
            }

            if ($number != 0) {
                $averageGrade = round($averageGrade / $number, 2);
            }
            $average[] = $averageGrade;
        }

        return $this->render('journal/index.html.twig', [
            'users' => $users,
            'tasks' => $tasks,
            'average' => $average,
        ]);
    }
}
