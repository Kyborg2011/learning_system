<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Subject;
use App\Entity\TaskType;

class CoursesController extends AbstractController
{
    /**
     * @Route("/subject/{id}", name="subject")
     */
    public function subject($id)
    {
        $subject = $this->getDoctrine()
            ->getRepository(Subject::class)
            ->find($id);

        $taskTypes = $this->getDoctrine()
            ->getRepository(TaskType::class)
            ->findAll();

        return $this->render('courses/subject.html.twig', [
            'subject' => $subject,
            'task_types' => $taskTypes,
        ]);
    }
}
