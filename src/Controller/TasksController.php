<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Subject;
use App\Entity\TaskType;
use App\Entity\Task;
use App\Entity\Solution;
use App\Form\SolutionType;

class TasksController extends AbstractController
{
    /**
     * @Route("/subject/{subjectId}/task_type/{id}", name="task_type")
     */
    public function taskType($id, $subjectId)
    {
        $subject = $this->getDoctrine()
            ->getRepository(Subject::class)
            ->find($subjectId);

        $taskType = $this->getDoctrine()
            ->getRepository(TaskType::class)
            ->find($id);

        return $this->render('task_types/index.html.twig', [
            'subject' => $subject,
            'task_type' => $taskType,
        ]);
    }

    /**
     * @Route("/subject/{subjectId}/task/{id}", name="single_task")
     */
    public function singleTask(Request $request, $id, $subjectId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $currentUser = $this->getUser();

        $subject = $this->getDoctrine()
            ->getRepository(Subject::class)
            ->find($subjectId);

        $task = $this->getDoctrine()
            ->getRepository(Task::class)
            ->find($id);

        $solution = new Solution();
        $form = $this->createForm(SolutionType::class, $solution);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['file']->getData();

            $fileName = $this->generateUniqueFileName() . '.' . $file->guessExtension();

            $file->move(
                __DIR__ . '/../../public/files/',
                $fileName
            );

            $solution->setFile($fileName);
            $solution->setTask($task);

            $entityManager->persist($solution);
            $entityManager->flush();
        }

        $qb = $entityManager->getRepository(Solution::class)->createQueryBuilder('p');
        $qb->andWhere('p.task = :task')
            ->andWhere('p.creator = :creator')
            ->setParameter('task', $task)
            ->setParameter('creator', $currentUser);
        $solution = $qb->getQuery()->getResult();
        if (is_array($solution) && count($solution)) {
            $solution = $solution[0];
        }
        
        return $this->render('task_types/single.html.twig', [
            'subject' => $subject,
            'task' => $task,
            'form' => $form->createView(),
            'solution' => $solution,
        ]);
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }
}
