<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Subject;
use App\Entity\Lecture;

class LecturesController extends AbstractController
{
    /**
     * @Route("/subject/{subjectId}/lecture", name="lecture")
     */
    public function lecture($subjectId)
    {
        $subject = $this->getDoctrine()
            ->getRepository(Subject::class)
            ->find($subjectId);

        return $this->render('lectures/index.html.twig', [
            'subject' => $subject,
        ]);
    }

    /**
     * @Route("/subject/{subjectId}/lecture/{id}", name="single_lecture")
     */
    public function singleLecture($subjectId, $id)
    {
        $subject = $this->getDoctrine()
            ->getRepository(Subject::class)
            ->find($subjectId);

        $lecture = $this->getDoctrine()
            ->getRepository(Lecture::class)
            ->find($id);

        return $this->render('lectures/single.html.twig', [
            'subject' => $subject,
            'lecture' => $lecture,
        ]);
    }
}
