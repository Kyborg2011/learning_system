<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Subject;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $subjects = $this->getDoctrine()
            ->getRepository(Subject::class)
            ->findAll();

        return $this->render('main/index.html.twig', [
            'subjects' => $subjects
        ]);
    }
}
