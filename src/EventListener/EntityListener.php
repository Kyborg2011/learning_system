<?php
namespace App\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Entity\Lecture;
use App\Entity\Message;
use App\Entity\Solution;
use App\Entity\Task;

class EntityListener
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage = null)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (null !== $currentUser = $this->getUser()) {
            if ($entity instanceof Lecture || $entity instanceof Solution || $entity instanceof Task) {
                $entity->setCreator($currentUser);
            }
            if ($entity instanceof Message) {
                $entity->setSender($currentUser);
            }
            if ($entity instanceof Solution) {
                $task = $entity->getTask();
                if ($task) {
                    $taskCreator = $task->getCreator();
                    $entity->setValuer($taskCreator);
                }
            }
        }
    }

    public function getUser()
    {
        if (!$this->tokenStorage) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        if (null === $token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }

        return $user;
    }
}
