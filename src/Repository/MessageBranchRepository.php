<?php

namespace App\Repository;

use App\Entity\MessageBranch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MessageBranch|null find($id, $lockMode = null, $lockVersion = null)
 * @method MessageBranch|null findOneBy(array $criteria, array $orderBy = null)
 * @method MessageBranch[]    findAll()
 * @method MessageBranch[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageBranchRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MessageBranch::class);
    }

    // /**
    //  * @return MessageBranch[] Returns an array of MessageBranch objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MessageBranch
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
