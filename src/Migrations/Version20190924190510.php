<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190924190510 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE task_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lecture (id INT AUTO_INCREMENT NOT NULL, subject_id INT NOT NULL, module_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_C167794823EDC87 (subject_id), INDEX IDX_C1677948AFC2B591 (module_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message_branch (id INT AUTO_INCREMENT NOT NULL, creator_id INT NOT NULL, recipient_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_8CA1861061220EA6 (creator_id), INDEX IDX_8CA18610E92F8F78 (recipient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE task (id INT AUTO_INCREMENT NOT NULL, subject_id INT DEFAULT NULL, module_id INT DEFAULT NULL, task_type_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_527EDB2523EDC87 (subject_id), INDEX IDX_527EDB25AFC2B591 (module_id), INDEX IDX_527EDB25DAADA679 (task_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE module (id INT AUTO_INCREMENT NOT NULL, subject_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_C24262823EDC87 (subject_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE solution (id INT AUTO_INCREMENT NOT NULL, task_id INT DEFAULT NULL, creator_id INT NOT NULL, valuer_id INT DEFAULT NULL, grade INT DEFAULT NULL, comment LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, checked_at DATETIME DEFAULT NULL, INDEX IDX_9F3329DB8DB60186 (task_id), INDEX IDX_9F3329DB61220EA6 (creator_id), INDEX IDX_9F3329DB67EC383F (valuer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, sender_id INT NOT NULL, message_branch_id INT NOT NULL, text LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_B6BD307FF624B39D (sender_id), INDEX IDX_B6BD307FB66FCA7F (message_branch_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE lecture ADD CONSTRAINT FK_C167794823EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id)');
        $this->addSql('ALTER TABLE lecture ADD CONSTRAINT FK_C1677948AFC2B591 FOREIGN KEY (module_id) REFERENCES module (id)');
        $this->addSql('ALTER TABLE message_branch ADD CONSTRAINT FK_8CA1861061220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE message_branch ADD CONSTRAINT FK_8CA18610E92F8F78 FOREIGN KEY (recipient_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB2523EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id)');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25AFC2B591 FOREIGN KEY (module_id) REFERENCES module (id)');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25DAADA679 FOREIGN KEY (task_type_id) REFERENCES task_type (id)');
        $this->addSql('ALTER TABLE module ADD CONSTRAINT FK_C24262823EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id)');
        $this->addSql('ALTER TABLE solution ADD CONSTRAINT FK_9F3329DB8DB60186 FOREIGN KEY (task_id) REFERENCES task (id)');
        $this->addSql('ALTER TABLE solution ADD CONSTRAINT FK_9F3329DB61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE solution ADD CONSTRAINT FK_9F3329DB67EC383F FOREIGN KEY (valuer_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FF624B39D FOREIGN KEY (sender_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FB66FCA7F FOREIGN KEY (message_branch_id) REFERENCES message_branch (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB25DAADA679');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FB66FCA7F');
        $this->addSql('ALTER TABLE solution DROP FOREIGN KEY FK_9F3329DB8DB60186');
        $this->addSql('ALTER TABLE lecture DROP FOREIGN KEY FK_C1677948AFC2B591');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB25AFC2B591');
        $this->addSql('DROP TABLE task_type');
        $this->addSql('DROP TABLE lecture');
        $this->addSql('DROP TABLE message_branch');
        $this->addSql('DROP TABLE task');
        $this->addSql('DROP TABLE module');
        $this->addSql('DROP TABLE solution');
        $this->addSql('DROP TABLE message');
    }
}
