<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190925142524 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lecture ADD creator_id INT DEFAULT NULL, ADD topic VARCHAR(255) NOT NULL, ADD goal VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE lecture ADD CONSTRAINT FK_C167794861220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_C167794861220EA6 ON lecture (creator_id)');
        $this->addSql('ALTER TABLE solution CHANGE creator_id creator_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE subject ADD description LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lecture DROP FOREIGN KEY FK_C167794861220EA6');
        $this->addSql('DROP INDEX IDX_C167794861220EA6 ON lecture');
        $this->addSql('ALTER TABLE lecture DROP creator_id, DROP topic, DROP goal');
        $this->addSql('ALTER TABLE solution CHANGE creator_id creator_id INT NOT NULL');
        $this->addSql('ALTER TABLE subject DROP description');
    }
}
