<?php
// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Solution", mappedBy="creator", orphanRemoval=true)
     */
    private $solutions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MessageBranch", mappedBy="creator", orphanRemoval=true)
     */
    private $messageBranches;

    public function __construct()
    {
        parent::__construct();
        $this->solutions = new ArrayCollection();
        $this->messageBranches = new ArrayCollection();
        // your own logic
    }

    /**
     * @return Collection|Solution[]
     */
    public function getSolutions(): Collection
    {
        return $this->solutions;
    }

    public function addSolution(Solution $solution): self
    {
        if (!$this->solutions->contains($solution)) {
            $this->solutions[] = $solution;
            $solution->setCreator($this);
        }

        return $this;
    }

    public function removeSolution(Solution $solution): self
    {
        if ($this->solutions->contains($solution)) {
            $this->solutions->removeElement($solution);
            // set the owning side to null (unless already changed)
            if ($solution->getCreator() === $this) {
                $solution->setCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MessageBranch[]
     */
    public function getMessageBranches(): Collection
    {
        return $this->messageBranches;
    }

    public function addMessageBranch(MessageBranch $messageBranch): self
    {
        if (!$this->messageBranches->contains($messageBranch)) {
            $this->messageBranches[] = $messageBranch;
            $messageBranch->setCreator($this);
        }

        return $this;
    }

    public function removeMessageBranch(MessageBranch $messageBranch): self
    {
        if ($this->messageBranches->contains($messageBranch)) {
            $this->messageBranches->removeElement($messageBranch);
            // set the owning side to null (unless already changed)
            if ($messageBranch->getCreator() === $this) {
                $messageBranch->setCreator(null);
            }
        }

        return $this;
    }
}
