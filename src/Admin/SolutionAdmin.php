<?php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Route\RouteCollection;
use App\Entity\Solution;
use App\Entity\User;
use App\Entity\Task;

final class SolutionAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $object = $this->getSubject();
        $formMapper
            ->add('task', EntityType::class, [
                'class'    => Task::class,
                'required' => true,
                'choice_label' => function ($task) {
                    return $task;
                }
            ])
            ->add('createdAt', null, [
                'format' => 'd.m.Y H:i',
            ])
            ->add('creator', EntityType::class, [
                'class'    => User::class,
                'required' => true,
                'choice_label' => function ($user) {
                    return $user->getFirstname() . ' ' . $user->getLastname();
                }
            ])
            ->add('file', FileType::class, [
                'data_class' => null,
                'required' => false,
                'disabled' => true,
                'help' => '<a target="_blank" href="/files/' . $object->getFile() . '">Solution document</a>',
            ])
            ->add('comment', TextareaType::class, [
                'required' => false,
            ])
            ->add('grade', TextType::class, [
                'required' => false,
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $builder = $this->getModelManager()->getEntityManager('App\Entity\Task')->getRepository(Task::class);
        $tasksResults = $builder->findAll();

        $tasksChoices = [];
        foreach ($tasksResults as $task) {
            $name = $task->getName();
            $tasksChoices[$name] = $task->getId();
        }

        $builder = $this->getModelManager()->getEntityManager('App\Entity\User')->getRepository(User::class);
        $usersResults = $builder->findAll();

        $usersChoices = [];
        foreach ($usersResults as $user) {
            $name = $user->getFirstname() . ' ' . $user->getLastname();
            $usersChoices[$name] = $user->getId();
        }

        $datagridMapper
            ->add('task',
                'doctrine_orm_choice',
                [],
                ChoiceType::class,
                [ 'choices' => $tasksChoices ]
            )
            ->add('creator',
                'doctrine_orm_choice',
                [],
                ChoiceType::class,
                [ 'choices' => $usersChoices ]
            )
            ->add('valuer',
                'doctrine_orm_choice',
                [],
                ChoiceType::class,
                [ 'choices' => $usersChoices ]
            )
            ->add('grade')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('task')
            ->add('creator')
            ->add('valuer')
            ->add('createdAt')
            ->add('grade')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery();
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');

        if (!$securityContext->isGranted('ROLE_SUPER_ADMIN')) {
            $query->join($query->getRootAlias() . '.task', 't')
                ->andWhere('t.creator = :current_user');
            $query->setParameter('current_user', $user);
        }

        return $query;
    }
}
