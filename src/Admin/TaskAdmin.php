<?php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Route\RouteCollection;
use App\Entity\TaskType;
use App\Entity\Module;

final class TaskAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class)
            ->add('content', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'tiny-mce-editor'
                ]
            ])
            ->add('taskType', EntityType::class, [
                'class'    => TaskType::class,
                'required' => true,
                'choice_label' => 'name',
                'placeholder' => 'Please select type of a task',
                'choice_label' => function ($taskType) {
                    return $taskType;
                }
            ])
            ->add('module', EntityType::class, [
                'class'    => Module::class,
                'required' => true,
                'choice_label' => 'name',
                'placeholder' => 'Please select subject and module of a task',
                'choice_label' => function ($module) {
                    return $module;
                }
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $builder = $this->getModelManager()->getEntityManager('App\Entity\Module')->getRepository(Module::class);
        $modulesResults = $builder->findAll();

        $modulesChoices = [];
        foreach ($modulesResults as $module) {
            $name = 'Subject: ' . $module->getSubject()->getName() . ' | Module: ' . $module->getName();
            $modulesChoices[$name] = $module->getId();
        }

        $builder = $this->getModelManager()->getEntityManager('App\Entity\TaskType')->getRepository(TaskType::class);
        $taskTypesResults = $builder->findAll();

        $taskTypesChoices = [];
        foreach ($taskTypesResults as $taskType) {
            $name = $taskType->getName();
            $taskTypesChoices[$name] = $taskType->getId();
        }

        $datagridMapper
            ->add('name')
            ->add('module',
                'doctrine_orm_choice',
                [],
                ChoiceType::class,
                [ 'choices' => $modulesChoices ]
            )
            ->add('taskType',
                'doctrine_orm_choice',
                [],
                ChoiceType::class,
                [ 'choices' => $taskTypesChoices ]
            )
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('taskType')
            ->add('module')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery();
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $securityContext = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');

        if (!$securityContext->isGranted('ROLE_SUPER_ADMIN')) {
            $query->andWhere($query->getRootAlias() . '.creator = :current_user');
            $query->setParameter('current_user', $user);
        }

        return $query;
    }
}
