<?php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Route\RouteCollection;
use App\Entity\Module;
use App\Repository\ModuleRepository;

final class LectureAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class)
            ->add('topic', TextType::class)
            ->add('goal', TextType::class)
            ->add('content', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'tiny-mce-editor'
                ]
            ])
            ->add('module', EntityType::class, [
                'class'    => Module::class,
                'required' => true,
                'choice_label' => 'name',
                'placeholder' => 'Please select module of a lecture',
                'choice_label' => function ($module) {
                    return $module;
                }
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $builder = $this->getModelManager()->getEntityManager('App\Entity\Module')->getRepository(Module::class);
        $modulesResults = $builder->findAll();

        $modulesChoices = [];
        foreach ($modulesResults as $module) {
            $name = 'Subject: ' . $module->getSubject()->getName() . ' | Module: ' . $module->getName();
            $modulesChoices[$name] = $module->getId();
        }

        $datagridMapper
            ->add('name')
            ->add('module',
                'doctrine_orm_choice',
                [],
                ChoiceType::class,
                [ 'choices' => $modulesChoices ]
            )
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('module')
            ->add('creator')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ]
            ])
        ;
    }
}
