<?php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Admin\AdminInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Route\RouteCollection;
use App\Entity\Subject;
use App\Repository\SubjectRepository;

final class ModuleAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class)
            ->add('description', TextareaType::class)
            ->add('subject', EntityType::class, [
                'class'    => Subject::class,
                'required' => true,
                'choice_label' => 'name',
                'placeholder' => 'Please select subject of a module',
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $builder = $this->getModelManager()->getEntityManager('App\Entity\Subject')->getRepository(Subject::class);
        $subjectsResults = $builder->findAll();

        $subjectsChoices = [];
        foreach ($subjectsResults as $subject) {
            $name = $subject->getName();
            $subjectsChoices[$name] = $subject->getId();
        }

        $datagridMapper
            ->add('name')
            ->add(
                'subject',
                'doctrine_orm_choice',
                [],
                ChoiceType::class,
                [ 'choices' => $subjectsChoices ]
            )
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', 'url', ['route' => [
                'name' => 'admin_app_module_lecture_list',
                'absolute' => true,
                'identifier_parameter_name' => 'id'
            ]])
            ->add('subject', null, [
                'associated_property' => 'name'
            ])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ]
            ])
        ;
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit', 'show'])) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        if ($this->isGranted('LIST')) {
            $menu->addChild('Manage Lectures', [
                'uri' => $admin->generateUrl('admin.lecture.list', [ 'id' => $id ])
            ]);
            $menu->addChild('Manage Tasks', [
                'uri' => $admin->generateUrl('admin.task.list', [ 'id' => $id ])
            ]);
        }
    }
}
